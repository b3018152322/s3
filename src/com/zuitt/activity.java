package com.zuitt;

import java.util.Scanner;

public class activity {
    public static void main(String[] args){

        Scanner numInput = new Scanner(System.in);
        int num1 = 0;

        System.out.println("Please input an integer whose factorial will be computed: ");

        try{
            num1 = numInput.nextInt();

            if(num1 == 0){
                System.out.println("The factorial of " + num1 + " is 1.");
            }
            else if(num1 > 0){

                // using for loop
//                long factorial1 = 1;
//                for(int i=1; i<= num1; i++){
//                    factorial1 = factorial1 * i;
//                }
//                System.out.println("The factorial of " + num1 + " is " + factorial1 + ".");

                // using while loop
                long factorial2 = 1;
                int j = 1;

                while(j <= num1){
                    factorial2 = factorial2 * j;
                    j++;
                }
                System.out.println("The factorial of " + num1 + " is " + factorial2 + ".");
            }
            else{
                System.out.println("Invalid input. This program only accepts zero and positive numbers.");
            }
        }
        catch(Exception e){
            System.out.println("Invalid input. This program only accepts zero and positive numbers.");
            e.printStackTrace();
        }

    }
}
